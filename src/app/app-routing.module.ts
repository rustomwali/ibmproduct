import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginComponent } from './components/login/login.component';
import { BarcodeComponent } from './components/barcode/barcode.component';
import { OriginSearchComponent } from './components/product/origin-search/origin-search.component';
import { OriginComponent } from './components/product/origin/origin.component';

const routes: Routes = [
  {
    path: '',
    component: LoginComponent,
  },
  {
    path: 'scan-barcode',
    component: BarcodeComponent
  },
  {
    path: 'origin-search',
    component: OriginSearchComponent
  },
  {
    path: 'origin',
    component: OriginComponent
  }
];


@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})

export class AppRoutingModule { }
