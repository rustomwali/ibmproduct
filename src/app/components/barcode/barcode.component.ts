import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { slideIn } from 'src/app/animations/slidein.animation';

@Component({
  selector: 'app-barcode',
  templateUrl: './barcode.component.html',
  styleUrls: ['./barcode.component.css'],
  animations: [slideIn]
})
export class BarcodeComponent implements OnInit {
  selectedPath = './assets/png/barcode.PNG';

  constructor(private router: Router) { }

  ngOnInit() {
  }

  navigateToSearch() {
    this.router.navigate(['origin-search']);
    return false;
  }
}
