import { Component, OnInit, AfterContentChecked } from '@angular/core';
import { Router } from '@angular/router';
import { environment } from '../../../environments/environment';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit, AfterContentChecked {
  navigateToDashboard: boolean;
  constructor(private router: Router) {

  }

  ngOnInit() { }

  ngAfterContentChecked() {

  }

  login() {
    this.router.navigate(['']);
  }


  signup() {
    //this.router.navigate(['/signup']);
  }

  logout() {
    //this.router.navigate(['']);
  }

  home() {
    //this.router.navigate(['']);
  }

  profile() {
    //this.router.navigate(['/profile']);
  }
}
