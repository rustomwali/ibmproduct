import { Component, ChangeDetectorRef, OnDestroy, OnInit, AfterContentChecked } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { slideIn } from 'src/app/animations/slidein.animation';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css'],
  animations: [slideIn]
})
export class LoginComponent implements OnInit, OnDestroy, AfterContentChecked {
  siginInForm: FormGroup;
  constructor(private formBuilder: FormBuilder,
    private chr: ChangeDetectorRef,
    private router: Router,
  ) { }

  ngOnInit() {
    this.siginInForm = this.formBuilder.group({
      email: ['', [Validators.required, Validators.email]],
      password: ['', [Validators.required, Validators.minLength(6)]]
    });
 
  }

  scan() {
    this.router.navigate(['/scan-barcode']);
    //this.router
    //this.store.dispatch(new UserLoginAction({ email, password }));
  }

  ngOnDestroy() {
    //this.storeSub.unsubscribe();
  }

  // this three methods is called after state is being changed ngDoCheck then ngAfterContentChecked then ngAfterViewChecked
  // ngAfterViewChecked() {}

  ngAfterContentChecked() {
   
  }
  // ngDoCheck() {}
}
