import { Component, OnInit } from '@angular/core';

import { slideIn } from 'src/app/animations/slidein.animation';

@Component({
  selector: 'app-origin',
  templateUrl: './origin.component.html',
  styleUrls: ['./origin.component.css'],
  animations: [slideIn]
})
export class OriginComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
