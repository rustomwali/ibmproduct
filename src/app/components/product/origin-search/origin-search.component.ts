import { Component, OnInit } from '@angular/core';
import { slideIn } from 'src/app/animations/slidein.animation';

@Component({
  selector: 'app-origin-search',
  templateUrl: './origin-search.component.html',
  styleUrls: ['./origin-search.component.css'],
  animations: [slideIn]
})
export class OriginSearchComponent implements OnInit {
  product = { name: 'Roye Soy Sauce', id: '3762800500' };
  expirationDate;

  constructor() { }

  ngOnInit() {
  }
}
