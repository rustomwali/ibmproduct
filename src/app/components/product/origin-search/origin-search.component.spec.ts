import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OriginSearchComponent } from './origin-search.component';

describe('OriginSearchComponent', () => {
  let component: OriginSearchComponent;
  let fixture: ComponentFixture<OriginSearchComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OriginSearchComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OriginSearchComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
